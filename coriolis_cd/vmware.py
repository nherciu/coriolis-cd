# Copyright 2018 Cloudbase Solutions Srl
# All Rights Reserved.

import argparse
import contextlib
import os
import ssl
import threading
import time

from urllib import request

from cliff.show import ShowOne

from pyVim import connect as pyVconnect
from pyVmomi import vim


DEFAULT_TEMPLATE_NAME = 'coriolis-demo-base'
DEFAULT_VM_NAME = 'cloudbase-demo'


def _log_msg(msg):
    """ Wrapper to log message. """
    print(msg)


def _keep_alive_vmware_conn(si, exit_event):
    try:
        while True:
            _log_msg("VMware connection keep alive")
            si.CurrentTime()
            if exit_event.wait(60):
                return
    finally:
        _log_msg("Exiting VMware connection keep alive thread")


@contextlib.contextmanager
def connect(host, username, password, port=443, allow_untrusted=False):
    """ Connects to the specified host and launches a thread
    to keep session alive. """
    context = ssl.SSLContext(ssl.PROTOCOL_SSLv23)
    if allow_untrusted:
        context.verify_mode = ssl.CERT_NONE

    _log_msg("Connecting to host: '%s'" % host)
    si = pyVconnect.SmartConnect(
        host=host,
        user=username,
        pwd=password,
        port=port,
        sslContext=context)

    thread = None
    try:
        thread_exit_event = threading.Event()
        thread = threading.Thread(
            target=_keep_alive_vmware_conn,
            args=(si, thread_exit_event))
        thread.start()

        yield context, si
    finally:
        pyVconnect.Disconnect(si)
        if thread:
            thread_exit_event.set()
            thread.join()


def _wait_for_task(task):
    """ Wait for a task to finish """
    while task.info.state not in [
            vim.TaskInfo.State.success, vim.TaskInfo.State.error]:
        time.sleep(1)
    if task.info.state == vim.TaskInfo.State.error:
        raise Exception(task.info.error.msg)


def _get_obj(
        content, vimtype, resource_name=None, error_on_not_found=True):
    """
    param resource_name: str: name of the resource - may be None, in which case
    the first resource of that type in the list is returned.
    param error_on_not_found: bool: whether to raise if not found
    """
    obj = None
    container = content.viewManager.CreateContainerView(
        content.rootFolder, vimtype, True)
    if resource_name:
        for c in container.view:
            if c.name == resource_name:
                obj = c
                break
    else:
        # if no name was given, we just return the first avilable
        # resource of this type:
        allobjs = list(container.view)
        if allobjs:
            return allobjs[0]
        else:
            raise Exception(
                "Could not find any resource of type '%s'" % vimtype)

    if obj is None and error_on_not_found:
        raise Exception(
            "Could not find item with name '%s' and type '%s'." % (
                resource_name, vimtype))

    return obj


def _get_vm(si, vm_name):
    # NOTE: retrieve fresh content:
    content = si.RetrieveContent()
    return _get_obj(
        content, [vim.VirtualMachine],
        resource_name=vm_name,
        error_on_not_found=True)


def _wait_for_vm_status(vmobj, status, period=10, max_wait=180):
    _log_msg("Waiting for VM '%s' status '%s'." % (vmobj.name, status))
    i = 0
    while i < max_wait and vmobj.runtime.powerState != status:
        _log_msg(
            "Waiting for VM '%s' status '%s'. Current status '%s'" % (
                vmobj.name, status, vmobj.runtime.powerState))
        time.sleep(period)
        i += period

    if i >= max_wait:
        raise Exception(
            "Failed waiting for VM '%s' to reach status '%s'. "
            "Current status is: '%s'" % (
                vmobj.name, status, vmobj.runtime.powerState))


def _clone_template(si, template_name, dc_name, new_vm_name, start_vm=True):
    if template_name == new_vm_name:
        raise ValueError(
            "'template_name' and 'new_vm_name' must not be equal.")
    content = si.RetrieveContent()

    # retrieve the VM template:
    template = _get_obj(content, [vim.VirtualMachine], template_name)

    # # prepare all cloning params:
    # get DC and root folder of DC:
    datacenter = _get_obj(content, [vim.Datacenter], dc_name)
    folder = datacenter.vmFolder

    # get datastore name off template:
    datastore = _get_obj(
        content, [vim.Datastore], template.datastore[0].info.name)

    # get first available cluster and its default resource pool:
    cluster = _get_obj(
        content, [vim.ClusterComputeResource], resource_name=None)
    resource_pool = cluster.resourcePool

    # set relocation and cloning specs:
    relocspec = vim.vm.RelocateSpec()
    relocspec.datastore = datastore
    relocspec.pool = resource_pool

    clonespec = vim.vm.CloneSpec()
    clonespec.location = relocspec
    clonespec.powerOn = start_vm

    new_vm = None
    try:
        _log_msg(
            "Cloning template '%s' as VM '%s'" % (
                template_name, new_vm_name))
        task = template.Clone(folder=folder, name=new_vm_name, spec=clonespec)
        _wait_for_task(task)

        new_vm = _get_vm(si, new_vm_name)
    except (Exception, KeyboardInterrupt):
        _check_delete_vm(si, new_vm_name)
        raise

    desired_state = vim.VirtualMachinePowerState.poweredOff
    if start_vm:
        desired_state = vim.VirtualMachinePowerState.poweredOn

    _wait_for_vm_status(new_vm, desired_state)

    return new_vm


def _check_vmware_tools_installed(vm_obj, raise_if_not=True):
    installed = (vm_obj.guest.toolsStatus !=
                 vim.vm.GuestInfo.ToolsStatus.toolsNotInstalled)

    if raise_if_not and not installed:
        raise Exception(
            "VMWare tools not installed in VM '%s'" % vm_obj.name)

    return installed


def _check_stop_vm(vm_obj):
    if vm_obj.runtime.powerState != vim.VirtualMachinePowerState.poweredOff:
        _log_msg("VM '%s' is in status '%s'. Powering off VM." % (
            vm_obj.name, vm_obj.runtime.powerState))
        poweroff_task = vm_obj.PowerOff()
        _wait_for_task(poweroff_task)

        _wait_for_vm_status(vm_obj, vim.VirtualMachinePowerState.poweredOff)
    else:
        _log_msg("VM '%s' is already off." % vm_obj.name)


def _wait_for_vm_ip_info(si, vm_name, period=10, timeout=180):
    """ Waits for the guest networking info to become available and
    returns the first IP address of the first NIC of the VM. """
    i = 0
    ip_address = None

    vm_obj = _get_vm(si, vm_name)
    _check_vmware_tools_installed(vm_obj, raise_if_not=True)

    _log_msg("Waiting for IP address info for VM '%s'." % vm_name)
    while i < timeout:
        nics = list(vm_obj.guest.net)
        if nics:
            nic = nics[0]
            ip_addresses = list(nic.ipAddress)
            if ip_addresses:
                _log_msg(
                    "IP addresses for NIC with MAC '%s': '%s'" % (
                        nic.macAddress, ip_addresses))

                ip_address = ip_addresses[0]
                break

        i = i + period
        # _log_msg(
        #     "Waiting for IP addresses for VM '%s'. NICs: %s" % (
        #         vm_name, nics))
        time.sleep(period)
        vm_obj = _get_vm(si, vm_name)

    if not ip_address:
        raise Exception(
            "Timed out after waiting %d seconds for IP address info "
            "for VM '%s'" % (period, vm_name))

    _log_msg("Using VM IP address '%s'" % ip_address)
    return ip_address


def _check_delete_vm(si, vm_name):
    # NOTE: retrieve fresh content:
    content = si.RetrieveContent()
    vm = _get_obj(
        content, [vim.VirtualMachine],
        resource_name=vm_name,
        error_on_not_found=False)

    if vm is None:
        _log_msg("Could locate VM '%s' for deletion; skipping." % vm_name)
        return

    _check_stop_vm(vm)

    _log_msg("Destroying VM '%s'" % vm_name)
    destroy_task = vm.Destroy_Task()
    _wait_for_task(destroy_task)


def _export_vm_appliance(si, context, hostname, vm_name, export_dir):
    """ Exports the contents of the VM in the given existing export dir.
    A '<vm_name>.ovf' file, alongside all the VM's VMDKs will be placed there.

    Returns the full path of the dir.
    """
    vm = _get_vm(si, vm_name)
    _check_stop_vm(vm)

    ovf_files = []
    lease = vm.ExportVm()
    # download all the disks:
    while True:
        if lease.state == vim.HttpNfcLease.State.ready:
            du = None
            try:
                tot_downloaded_bytes = 0
                for du in [du for du in lease.info.deviceUrl if du.disk]:
                    size_bytes_dev = 0
                    # Key format: '/vm-70/VirtualLsiLogicController0:0'
                    ctrl_str, _ = du.key[
                        du.key.rindex('/') + 1:].split(':')

                    def _get_class_name(obj):
                        return obj.__class__.__name__.split('.')[-1]

                    for i, ctrl in enumerate(
                        [d for d in vm.config.hardware.device if
                         isinstance(
                            d, vim.vm.device.VirtualController) and
                            ctrl_str.startswith(_get_class_name(d))]):
                        if int(ctrl_str[len(_get_class_name(ctrl)):]) == i:
                            break

                    # NOTE: when exporting directly from an ESXi host,
                    # the hostname in the URL will be '*'
                    disk_url = du.url.replace("*", hostname)
                    response = request.urlopen(disk_url, context=context)
                    path = os.path.join(export_dir, du.targetId)

                    _log_msg("Downloading disk to path '%s'" % path)
                    with open(path, 'wb') as f:
                        while True:
                            chunk = response.read(1024 * 1024)
                            if not chunk:
                                break
                            tot_downloaded_bytes += len(chunk)
                            size_bytes_dev += len(chunk)
                            f.write(chunk)
                            lease_progress = int(
                                tot_downloaded_bytes * 100 / (
                                    lease.info.totalDiskCapacityInKB * 1024))
                            _log_msg("Lease progress: %d%%" % lease_progress)
                            lease.HttpNfcLeaseProgress(lease_progress)

                    ovf_file = vim.OvfManager.OvfFile()
                    ovf_file.deviceId = du.key
                    # NOTE: path must be relative from root of the OVA dir:
                    ovf_file.path = du.targetId
                    ovf_file.size = size_bytes_dev
                    ovf_files.append(ovf_file)

                lease.HttpNfcLeaseComplete()
                break
            except (Exception, KeyboardInterrupt) as ex:
                disk_name = "unknown"
                if du is not None:
                    disk_name = du.url
                _log_msg(
                    "Exception ocurred for disk '%s'. Relinquishing lease."
                    " Exception: %s" % (disk_name, str(ex)))
                lease.HttpNfcLeaseAbort()
                raise
        elif lease.state == vim.HttpNfcLease.State.error:
            raise Exception(lease.error.msg)
        else:
            _log_msg("Current export lease state is: '%s'" % lease.state)
            time.sleep(.1)

    # create OVF metafile:
    _log_msg("Creating OVF metafile for VM.")
    ovf_descriptor_name = vm.name
    ovf_params = vim.OvfManager.CreateDescriptorParams()
    ovf_params.name = ovf_descriptor_name
    ovf_params.ovfFiles = ovf_files

    ovf_manager = si.content.ovfManager
    ovf_descriptor = ovf_manager.CreateDescriptor(
        obj=vm, cdp=ovf_params)

    if ovf_descriptor.error:
        raise Exception(ovf_descriptor.error[0].fault)
    else:
        # write descriptor file:
        ovf_descriptor_data = ovf_descriptor.ovfDescriptor
        ovf_descriptor_path = os.path.join(
            export_dir, "%s.ovf" % ovf_descriptor_name)
        with open(ovf_descriptor_path, 'wb') as fout:
            fout.write(ovf_descriptor_data.encode('utf-8'))

    return os.path.abspath(export_dir)

class Boot(ShowOne):
    """Spawn an instance on vmware"""

    def get_parser(self, prog_name):
        parser = super(Boot, self).get_parser(prog_name)
        parser.add_argument('--host', dest="host", required=True,
                            help='Hostname/address of the ESXi/vSphere host')
        parser.add_argument('--port', type=int, dest="port", default=443,
                            help='Port on the host to connect to.')
        parser.add_argument('--allow-untrusted', action="store_true",
                            dest='allow_untrusted',
                            help="Whether to open sessions with self-signed certs")
        parser.add_argument('--username', dest="username", required=True,
                            help='ESXi/vSphere username.')
        parser.add_argument('--password', dest="password",
                            help='ESXi/vSphere password', required=True)
        parser.add_argument('--dc-name', dest="dc_name",
                            help='Datacenter name', required=True)
        parser.add_argument('--template-name', dest="template_name",
                            help='Name of OL7 template with guest tools installed,'
                                 ' and prepped for the coriolis-docker deplyment',
                            default=DEFAULT_TEMPLATE_NAME)
        parser.add_argument('--vm-name', dest="vm_name", default=DEFAULT_VM_NAME,
                            help="String name for the VM on vSphere.")
        return parser

    def take_action(self,args):
        new_vm_name = args.vm_name
        datacenter_name = args.dc_name
        template_name = args.template_name

        new_vm = None
        ovf_contents_dir = None

        with connect(args.host, args.username, args.password,
                    port=args.port, allow_untrusted=args.allow_untrusted) as (
                        context, si):
            try:
                new_vm = _clone_template(
                    si, template_name, datacenter_name, new_vm_name,
                    start_vm=True)
                vm_ip = _wait_for_vm_ip_info(si, new_vm_name)
            finally:
                _log_msg("NOT DELETING VM")
                # _check_delete_vm(si, new_vm_name)

        columns = ('VM IP',)
        data = (vm_ip,)

        return (columns, data)

class Export(ShowOne):
    """Export an instance from vmware"""

    def get_parser(self, prog_name):
        parser = super(Export, self).get_parser(prog_name)
        parser.add_argument('--host', dest="host", required=True,
                            help='Hostname/address of the ESXi/vSphere host')
        parser.add_argument('--port', type=int, dest="port", default=443,
                            help='Port on the host to connect to.')
        parser.add_argument('--allow-untrusted', action="store_true",
                            dest='allow_untrusted',
                            help="Whether to open sessions with self-signed certs")
        parser.add_argument('--username', dest="username", required=True,
                            help='ESXi/vSphere username.')
        parser.add_argument('--password', dest="password",
                            help='ESXi/vSphere password', required=True)
        parser.add_argument('--dc-name', dest="dc_name",
                            help='Datacenter name', required=True)
        parser.add_argument('--target-path', dest="target_path",
                            help='Target OVA path', required=True)
        parser.add_argument('--vm-name', dest="vm_name", default=DEFAULT_VM_NAME,
                            help="String name for the VM on vSphere.")
        return parser

    def take_action(self, args):
        _log_msg("exporting VM")
        exported_vm_name = args.vm_name
        datacenter_name = args.dc_name

        target_dir = args.target_path
        export_dir = os.path.join(target_dir, exported_vm_name.replace(" ",""))
        if not os.path.exists(export_dir) and os.path.exists(target_dir):
            os.mkdir(export_dir)
        if not os.path.isdir(export_dir) or os.listdir(export_dir):
            raise Exception(
                "The export directory '%s' must exist and be empty." % export_dir)

        ovf_contents_dir = None
        with connect(args.host, args.username, args.password,
                    port=args.port, allow_untrusted=args.allow_untrusted) as (
                        context, si):
            try:
                vm = _get_vm(si, exported_vm_name)
                _check_stop_vm(vm)
                ovf_contents_dir = _export_vm_appliance(
                    si, context, args.host, vm.name, export_dir)
            finally:
                #_log_msg("exported VM %s" % exported_vm_name)
                _log_msg("NOT DELETING VM")
                # _check_delete_vm(si, new_vm_name)

        columns = ('VM exported to',)
        data = (export_dir,)

        return (columns, data)

