# Copyright 2018 Cloudbase Solutions Srl
# All Rights Reserved.
import paramiko

def _log_msg(msg):
    """ Wrapper to log message. """
    print(msg)


def _get_ssh_client(ip, vm_username, vm_password):
    _log_msg("Connecting to host: '%s'" % ip)
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(
        paramiko.AutoAddPolicy())
    ssh.connect(ip, username=vm_username, password=vm_password)
    return ssh


def _exec_ssh_cmd(ssh, cmd, check_exit_code=True):
    stdin, stdout, stderr = ssh.exec_command(cmd)
    std_out = stdout.read().decode()
    std_err = stderr.read().decode()
    _log_msg(std_out)
    _log_msg(std_err)
    exit_code = stdout.channel.recv_exit_status()
    if check_exit_code and exit_code != 0:
        raise Exception("Command exited with code: %s" % exit_code)
    return std_out

def _send_file(ssh, localfile, remotefile):
    transport = ssh.get_transport()
    sftp_client = transport.open_sftp_client()
    sftp_client.put(localfile, remotefile)
