# Copyright 2019 Cloudbase Solutions Srl
# All Rights Reserved.

import sys

from cliff.app import App
from cliff.commandmanager import CommandManager


class CoriolisCdApp(App):

    def __init__(self):
        super(CoriolisCdApp, self).__init__(
            description='Tool for generating coriolis appliances',
            version='0.1',
            command_manager=CommandManager('coriolis.cd'),
            deferred_help=True,
            )


def main(argv=sys.argv[1:]):
    myapp = CoriolisCdApp()
    return myapp.run(argv)


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))

