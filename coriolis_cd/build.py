# Copyright 2018 Cloudbase Solutions Srl
# All Rights Reserved.

from cliff.command import Command
from coriolis_cd import utils as coriolis_cd_utils

def _run_coriolis_build(
        ssh, key, rem_key, registry, registry_username, registry_password, export_list, import_list):
    coriolis_cd_utils._log_msg("Logging into Docker registry: '%s'" % registry)
    coriolis_cd_utils._exec_ssh_cmd(
        ssh,
        'docker login %s -u %s -p "%s"' % (
            registry, registry_username, registry_password))

    coriolis_cd_utils._log_msg("Cloning coriolis-docker")
    coriolis_cd_utils._exec_ssh_cmd(
        ssh,
        "git clone https://bitbucket.org/nherciu/coriolis-docker.git -b provlist")
    # "git clone https://bitbucket.org/aznashwan/coriolis-docker.git -b azure-marketplace-image")

    # TODO(aznashwan): find better workaround to cloning the kolla-ansible repo
    coriolis_cd_utils._exec_ssh_cmd(
        ssh, 'git config --global user.email "coriolis-builds@cloudbase.com"')
    coriolis_cd_utils._exec_ssh_cmd(ssh, 'git config --global user.name "Coriolis Builds"')

    # NOTE: pip >= 10 will refuse to uninstall dist-utils installed packages
    # during the final cleanup phase of kolla/deploy.sh (there are a handful of
    # packages which are installed by yum as part of the
    # `yum install ansible` done in `prereqs_ol7.sh`)
    coriolis_cd_utils._exec_ssh_cmd(ssh, "pip install --upgrade --force-reinstall 'pip<10'")
    coriolis_cd_utils._exec_ssh_cmd(ssh, "pip install --upgrade --force-reinstall decorator")

    coriolis_cd_utils._log_msg("Creating coriolis-docker build config file")
    coriolis_cd_utils._exec_ssh_cmd(ssh, "cp /root/coriolis-docker/config-build.yml.sample /root/coriolis-docker/config-build.yml")
    coriolis_cd_utils._exec_ssh_cmd(ssh, 'sed -i "s/^export_providers:.*/export_providers: %s/g" /root/coriolis-docker/config-build.yml' % export_list)
    coriolis_cd_utils._exec_ssh_cmd(ssh, 'sed -i "s/^import_providers:.*/import_providers: %s/g" /root/coriolis-docker/config-build.yml' % import_list)
    coriolis_cd_utils._exec_ssh_cmd(ssh, 'sed -i "s|^bitbucket_key_path:.*|bitbucket_key_path: %s|g" /root/coriolis-docker/config-build.yml' % rem_key)

    coriolis_cd_utils._log_msg("Setting up bitbucket keys")
    coriolis_cd_utils._send_file(ssh, key, rem_key)
    coriolis_cd_utils._exec_ssh_cmd(ssh, "chmod 600 %s" % rem_key)
    coriolis_cd_utils._exec_ssh_cmd(ssh, "mkdir ~/.ssh && ssh-keyscan -H bitbucket.org >> ~/.ssh/known_hosts")
    ssh_agent_output = coriolis_cd_utils._exec_ssh_cmd(ssh, "ssh-agent")
    ssh_agent_output = ssh_agent_output.split(";")
    export_ssh_auth_sock = "export "+ssh_agent_output[0]
    export_ssh_agent_pid = "export "+ssh_agent_output[2][1:]

    coriolis_cd_utils._log_msg("Building Coriolis licensing server")
    coriolis_cd_utils._exec_ssh_cmd(ssh, "%s; %s; ssh-add %s; /root/coriolis-docker/licensing/build.sh" % (
        export_ssh_auth_sock, export_ssh_agent_pid, rem_key))

    coriolis_cd_utils._log_msg("Building Coriolis images")
    coriolis_cd_utils._exec_ssh_cmd(ssh, "%s; %s; ssh-add %s; /root/coriolis-docker/build.sh" % (
        export_ssh_auth_sock, export_ssh_agent_pid, rem_key))

    coriolis_cd_utils._log_msg("Cleaning up and logging out.")
    coriolis_cd_utils._exec_ssh_cmd(ssh, "docker logout %s")
    coriolis_cd_utils._exec_ssh_cmd(ssh, "rm -f /root/bitbucket_key")
    coriolis_cd_utils._exec_ssh_cmd(ssh, "if [ -f /root/bitbucket_key ]; then echo 'Bitbucket key not deleted' 1>&2 && exit 1; fi")
    coriolis_cd_utils._exec_ssh_cmd(ssh, "rm -f ~/.bash_history && history -c")


class Build(Command):
    """Deploy coriolis on an existing VM"""

    def get_parser(self, prog_name):
        parser = super(Build, self).get_parser(prog_name)
        parser.add_argument('--host', dest="host", required=True,
                            help='Hostname/address of the VM')
        parser.add_argument('--username', dest="username", required=True,
                            help='VM username.')
        parser.add_argument('--password', dest="password",
                            help='VM password', required=True)
        parser.add_argument('--key-path', dest="key_path",
                            help='Path to bitbucket key', required=True)
        parser.add_argument('--remote-key-path', dest="rem_key_path",
                            help='Path to upload the bitbucket key to on the VM', required=True)
        parser.add_argument('--docker-registry', dest="registry_host",
                            help='Docker registry', required=True)
        parser.add_argument('--docker-registry-user', dest="registry_username",
                            help='Docker registry user', required=True)
        parser.add_argument('--docker-registry-password',
                            dest="registry_password",
                            help='Docker registry user', required=True)
        parser.add_argument('--export-providers', required=True,
                            dest="export_providers", default=[], nargs='+',
                            help='List of export providers to be added to the image')
        parser.add_argument('--import-providers', required=True,
                            dest="import_providers", default=[], nargs='+',
                            help='List of import providers to be added to the image')
        return parser

    def take_action(self, args):
        vm_ip = args.host
        vm_user = args.username
        vm_pass = args.password
        bitbucket_key_path = args.key_path
        remote_bitbucket_key_path = args.rem_key_path
        registry = args.registry_host
        registry_username = args.registry_username
        registry_password = args.registry_password
        exp_providers_list=args.export_providers
        imp_providers_list=args.import_providers

        vm_ssh = coriolis_cd_utils._get_ssh_client(vm_ip, vm_user, vm_pass)
        _run_coriolis_build(
            vm_ssh, bitbucket_key_path, remote_bitbucket_key_path, registry, registry_username, registry_password, exp_providers_list, imp_providers_list)

