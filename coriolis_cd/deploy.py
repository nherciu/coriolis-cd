# Copyright 2018 Cloudbase Solutions Srl
# All Rights Reserved.

from cliff.command import Command
from coriolis_cd import utils as coriolis_cd_utils


def _run_coriolis_deployment(
        ssh, registry, registry_username, registry_password, pull_images):
    coriolis_cd_utils._log_msg("Logging into Docker registry: '%s'" % registry)
    coriolis_cd_utils._exec_ssh_cmd(
        ssh,
        'docker login %s -u %s -p "%s"' % (
            registry, registry_username, registry_password))

    coriolis_cd_utils._log_msg("Checking for existing docker-ansible configs")
    coriolis_cd_utils._exec_ssh_cmd(
        ssh,
        "mkdir /root/config-backups")
    coriolis_cd_utils._exec_ssh_cmd(
        ssh,
        "if [ -f /root/coriolis-docker/config-build.yml ]; then cp /root/coriolis-docker/config-build.yml /root/config-backups/; fi")
    coriolis_cd_utils._exec_ssh_cmd(
        ssh,
        "if [ -f /root/coriolis-docker/config.yml ]; then cp /root/coriolis-docker/config.yml /root/config-backups/; fi")
    coriolis_cd_utils._exec_ssh_cmd(
        ssh,
        "if [ -d /root/coriolis-docker/ ]; then rm -rf /root/coriolis-docker/; fi")

    coriolis_cd_utils._log_msg("Cloning coriolis-docker")
    coriolis_cd_utils._exec_ssh_cmd(
        ssh,
        "git clone https://bitbucket.org/nherciu/coriolis-docker.git -b provlist")
    # "git clone https://bitbucket.org/aznashwan/coriolis-docker.git -b azure-marketplace-image")
    coriolis_cd_utils._log_msg("Checking for docker-ansible configs to restore")
    coriolis_cd_utils._exec_ssh_cmd(
        ssh,
        "if [ -f /root/config-backups/config-build.yml ]; then cp /root/config-backups/config-build.yml /root/coriolis-docker/; fi")
    coriolis_cd_utils._exec_ssh_cmd(
        ssh,
        "if [ -f /root/config-backups/config.yml ]; then cp /root/config-backups/config.yml /root/coriolis-docker/; else cp /root/coriolis-docker/config.yml.sample /root/coriolis-docker/config.yml; fi")

    coriolis_cd_utils._log_msg("Setting pull value in deploy config")
    coriolis_cd_utils._exec_ssh_cmd(ssh, 'sed -i "s/^pull_coriolis_docker_images:.*/pull_coriolis_docker_images: %s/g" /root/coriolis-docker/config.yml' % pull_images)
    # TODO(aznashwan): find better workaround to cloning the kolla-ansible repo
    coriolis_cd_utils._exec_ssh_cmd(
        ssh, 'git config --global user.email "coriolis-builds@cloudbase.com"')
    coriolis_cd_utils._exec_ssh_cmd(ssh, 'git config --global user.name "Coriolis Builds"')

    # NOTE: pip >= 10 will refuse to uninstall dist-utils installed packages
    # during the final cleanup phase of kolla/deploy.sh (there are a handful of
    # packages which are installed by yum as part of the
    # `yum install ansible` done in `prereqs_ol7.sh`)
    coriolis_cd_utils._exec_ssh_cmd(ssh, "pip install --upgrade --force-reinstall 'pip<10'")
    coriolis_cd_utils._exec_ssh_cmd(ssh, "pip install --upgrade --force-reinstall decorator")

    coriolis_cd_utils._log_msg("Deploying Kolla")
    coriolis_cd_utils._exec_ssh_cmd(ssh, "/root/coriolis-docker/kolla/deploy.sh")

    coriolis_cd_utils._log_msg("Deploying Coriolis")
    coriolis_cd_utils._exec_ssh_cmd(ssh, "/root/coriolis-docker/deploy.sh")

    coriolis_cd_utils._log_msg("### NOTE: re-running deploy.sh for proxy container")
    coriolis_cd_utils._exec_ssh_cmd(ssh, "/root/coriolis-docker/deploy.sh")

    coriolis_cd_utils._log_msg("Deploying Coriolis licensing server")
    coriolis_cd_utils._exec_ssh_cmd(ssh, "/root/coriolis-docker/licensing/deploy.sh")

    coriolis_cd_utils._log_msg("Resetting licensing database")
    coriolis_cd_utils._exec_ssh_cmd(ssh, "/root/coriolis-docker/licensing/reset-licensing-db.sh")

    # _log_msg("Installing and configuring WALinuxAgent")
    # _exec_ssh_cmd(ssh, "/root/coriolis-docker/utilities/install_walinux_agent.sh")

    # _log_msg("Removing VMWare tools")
    # _exec_ssh_cmd(ssh, "yum remove -y open-vm-tools")

    coriolis_cd_utils._log_msg("Cleaning up and logging out.")
    coriolis_cd_utils._exec_ssh_cmd(ssh, "docker logout %s")
    coriolis_cd_utils._exec_ssh_cmd(ssh, "rm -rf /root/config-backups/")
    coriolis_cd_utils._exec_ssh_cmd(ssh, "rm -f ~/.bash_history && history -c")


class Deploy(Command):
    """Deploy coriolis on an existing VM"""

    def get_parser(self, prog_name):
        parser = super(Deploy, self).get_parser(prog_name)
        parser.add_argument('--host', dest="host", required=True,
                            help='Hostname/address of the VM')
        parser.add_argument('--username', dest="username", required=True,
                            help='VM username.')
        parser.add_argument('--password', dest="password",
                            help='VM password', required=True)
        parser.add_argument('--docker-registry', dest="registry_host",
                            help='Docker registry', required=True)
        parser.add_argument('--docker-registry-user', dest="registry_username",
                            help='Docker registry user', required=True)
        parser.add_argument('--docker-registry-password',
                            dest="registry_password",
                            help='Docker registry user', required=True)
        parser.add_argument('--docker-pull-images', dest="docker_pull",
                            help='Set this to "yes" for upstream images or "no" if images were built on this VM', required=True)
        return parser

    def take_action(self, args):
        vm_ip = args.host
        vm_user = args.username
        vm_pass = args.password
        registry = args.registry_host
        registry_username = args.registry_username
        registry_password = args.registry_password
        pull_images = args.docker_pull

        vm_ssh = coriolis_cd_utils._get_ssh_client(vm_ip, vm_user, vm_pass)
        _run_coriolis_deployment(
            vm_ssh, registry, registry_username, registry_password, pull_images)

