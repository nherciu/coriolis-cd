# Coriolis vSphere appliance deployment/delivery tools #

This is a tool for generating coriolis appliances. It supports booting VMs on
vmware, installing coriolis on VMs (no matter where they are hosted as long as
they are reachable via SSH) and exporting VMs from vmware.

Booting a VM:

    usage: corioliscd vmware boot [-h] [-f {json,shell,table,value,yaml}]
                                  [-c COLUMN] [--noindent] [--prefix PREFIX]
                                  [--max-width <integer>] [--fit-width]
                                  [--print-empty] --host HOST [--port PORT]
                                  [--allow-untrusted] --username USERNAME
                                  --password PASSWORD --dc-name DC_NAME
                                  [--template-name TEMPLATE_NAME]
                                  [--vm-name VM_NAME]

Deploying Coriolis on a VM:

    usage: corioliscd deploy [-h] --host HOST --username USERNAME --password
                             PASSWORD --docker-registry REGISTRY_HOST
                             --docker-registry-user REGISTRY_USERNAME
                             --docker-registry-password REGISTRY_PASSWORD

Exporting a VM from vmware:

    usage: corioliscd vmware export [-h] [-f {json,shell,table,value,yaml}]
                                    [-c COLUMN] [--noindent] [--prefix PREFIX]
                                    [--max-width <integer>] [--fit-width]
                                    [--print-empty] --host HOST [--port PORT]
                                    [--allow-untrusted] --username USERNAME
                                    --password PASSWORD --dc-name DC_NAME
                                    --target-path TARGET_PATH [--vm-name VM_NAME]
