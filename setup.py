#!/usr/bin/env python

from setuptools import setup

setup(
    name='corioliscd',
    version='0.1',
    description='Tool for generating coriolis appliances',
    author='CloudBase Solutions',
    entry_points={
        'console_scripts': [
            'corioliscd = coriolis_cd.shell:main'
        ],
        'coriolis.cd': [
            'vmware_boot = coriolis_cd.vmware:Boot',
            'vmware_export = coriolis_cd.vmware:Export',
            'deploy = coriolis_cd.deploy:Deploy',
            'build = coriolis_cd.build:Build',
        ],
    },
    install_requires=['paramiko', 'pyVmomi>=6.5', 'pyOpenSSL', 'cliff'],
)

